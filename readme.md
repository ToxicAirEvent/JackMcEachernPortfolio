#A Portfolio Framework

When I was looking to update my portfolio I felt I needed a new website that would be better adaptable for the future. Something that would allow me to quickly and easily add new pages for new projects I have worked on.

I wanted it to be relatively lightweight yet still have some amount of dynamic flexability across pages to allow standardization of content elements.

From this my portfolio framework was born. You can see it in action on my portfolio website [jackmceachern.com](https://jackmceachern.com/ "jackmceachern.com").

The framework is built using php and then utilizes template documents to standardize content pieces like the header and footer. It also allows for base site and CDN URL support across all pages.

There are dozens of good frameworks out there on the internet for web development across many different programming languages. However I choose to write my own for my portfolio as part of an ongoing learning experience. In some ways I felt like the simplicity of the overall code base for this is easier managed to as opposed to installing an entire more large scale framework like [laravel](https://laravel.com/ "laravel"), [django](https://www.djangoproject.com/ "django"), or [Angular](https://angular.io/ "Angular"), among the many others out there.

## I want to use this. Can I?

Sure!

Just clone in this repository and clear out the /pages directory of every file except for home.php.

You will also need to empty all the contents of the /img directory.

You can then use the /pages directory to design and build your own pages for your website.

#### Some Notes:

- Be sure to update the resource_locators.php file as well to use your sites URL and CDN URL. If you do not have your own CDN simply use your URL twice, or when you want to call on content pieces just use your site URL.
- - In any page or /base element you can use <?=$site_url ?> to get your sites URL or <?=$cdn_url ?> to get your CDN URL. If you ever more your site to another domain all you have to do is update your resource locators and your entire site will be updated to use them.
- Pages are defined in the /pages directory and can be accessed by going to https://YourSite.com/page/NameOfPageFileWithoutExtension. Any file type is valid as a page. It will prefer html files ahead of other file types. Be careful not to duplicate file names.
- If you want to change the style fo the site you can edit the CSS in the /css folder. Javascript is stored in the /js folder.
- Rearranging the header, footer, or other page elements is easy. They are located in the /base folder and named eponymously. Edit the header file to modify the header, footer to modify the footer, and so on.
- Meta data for the pages is set using javascript. Each page should contain the following json snippet. 

    <script type="application/json" id="page_meta">
    	{
    		"page_title": "Assorted Graphic Design",
    		"page_description" : "Over the years Jack McEachern has worked with a number of clients to create unique graphic designs. Here is a sample of a few of his works. Learn more and view."
    	}
    </script>
	

With its information updated to describe the content of the page.

## To do:
- Come up with a better way to set page meta data. Currently it is loaded through a json element and javascript that is run on each page. Google will currently index this but it isn't a best practice. There are better ways to buffer variables containing this data which can then be echoed out into the page using php so they are actual html elements at the time of request and not relying on javascript to do the dirty work.
- Split this framework away from my personal work a little better. It would be ideal to provide a 'blank' site repository for download as opposed to the current state which houses all my work.