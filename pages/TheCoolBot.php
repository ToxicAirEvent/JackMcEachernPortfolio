<div class="container">
	<div class="project-title">
		<h2>The Cool Bot</h2>
		<h3>Node.js Development - 2017 &amp; 2018</h3>
	</div>
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p>
				The Cool Bot was developed using <a href="https://nodejs.org/en/" target="_blank">Node.js</a> with <a href="https://gitlab.com/ToxicAirEvent/TheCoolBot" target="_blank">it's repository available for public use on my GitLab page.</a>
			</p>

			<p>
				The bot serves a simple purpose in that it interfaces with the Discord API in order to send chat messages back to users to relay game server information. This was developed as a tool for friends I play games with.  We often wanted to see what was on specific servers. This allows new users to easily get the server information in an automated way. The bot accomplishes that using triggers which tell it which server to query and return data from.
			</p>
	
			<p>
				Recently I updated the bot to support multiple servers, entered by any user who installs the bot. Hopefully this makes the bot more versatile for people who might want to use it outside of my group of friends.
			</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
				<img src="<?=$cdn_url ?>/builtWith/Node.png" class="img-responsive">
		</div>
	</div>
</div>

<div class="container">
	<img src="<?=$cdn_url ?>/TheCoolBot/TheCoolBot_InServer.png" class="work-column left">
	<img src="<?=$cdn_url ?>/TheCoolBot/BotChatOutputExample.png" class="work-column left">
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "The Cool Bot",
		"page_description" : "The Cool Bot is a Discord bot built using Node.js which will query and return data about game servers to the chat allowing users to quickly see who is playing what and how to connect to the server. Learn more."
	}
</script>
