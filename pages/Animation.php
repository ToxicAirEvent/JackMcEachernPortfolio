<div class="container">
	<div class="project-title">
		<h2>Animation</h2>
		<h3>Autodesk Maya - 2015 &amp; 2016</h3>
	</div>

	<div class="project-details-container breathing-room">
		<h5>About:</h5>
		<p>While in college, I had the opportunity to take several animation classes.  These classes allowed me exposure to software like Autodesk Maya.</p>
		<p>I completed several short projects exposing me to environment fly-through, title sequencing, and just simple 3D object/product rotations.
		It was a great introduction to some more advanced concepts in 3D animation.</p>
	</div>
</div>

<div class="expanded-work" id="content">
	<video class="img-responsive project-images" controls>
	  <source src="<?=$site_url ?>/vid/Jack-McEachern_Jaws-19_Title-sequence-animation.mp4" type="video/mp4">
	Your browser does not support the video tag.
	</video>
	
	<video class="img-responsive project-images" controls>
	  <source src="<?=$site_url ?>/vid/Jack-McEachern_Custom-Object-Spinning_Final.mp4" type="video/mp4">
	Your browser does not support the video tag.
	</video>
	
	<video class="img-responsive project-images" controls>
	  <source src="<?=$site_url ?>/vid/Enviornment-Fly-Through_1_Compiled-Frames.mp4" type="video/mp4">
	Your browser does not support the video tag.
	</video>
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Animations",
		"page_description" : "A series of animations made by Jack McEachern while in college. All animations were created and rendered using AutoDesk Maya with other assetsbeing created in Adobe Photoshop and Adobe Illustrator. Learn more and watch."
	}
</script>
