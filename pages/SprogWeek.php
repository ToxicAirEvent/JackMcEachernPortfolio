<div class="container">
	<div class="project-title">
		<h2>Sprog Week</h2>
		<h3>Web Design &amp; Wordpress Theming - 2016</h3>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/SprogWeek/HomePage2020.png" class="image-responsive" />
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p><a href="http://www.sproginc.org/home.html" target="_blank">Sprog Inc</a> is a children's charity dedicated to sending underserved children in the Kirkwood, Missouri, community to an academic summer camp.</p>
			<p>As part of a fundraising campaign in 2016, 'SPROG WEEK' was designed. This was a seven day project wherein members of the community hosted events raising funds and awareness of the program.</p>
			<p>The organizers needed a website that was easily updated and accessible allowing community members quickly know what events were happening at various locations in the city.</p>
			<p>This was done by simply making each 'event' a new 'post' as part of the Wordpress taxonomy. Each day of the week was then created as a category, and the homepage of the site is simply a custom piece of code to list the post within each category under and expandable heading. It ended up being a fairly elegant solution without the need for extra plugins, or extensions. It just uses existing theme structure and functionality of core Wordpress.</p>
			<p>Sprog Week and it's website have come and gone. To view an archive you can find one at <a href="https://sprogweek.jackmceachern.com/" target="_blank">sprogweek.jackmceachern.com</a>.</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
			<img src="<?=$cdn_url ?>/builtWith/WordpressLogo.png" class="img-responsive">
		</div>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/SprogWeek/ChurchTour.png" class="work-column left">
	<img src="<?=$cdn_url ?>/SprogWeek/WhatCanYouDo.png" class="work-column right">
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Sprog Week",
		"page_description" : "The organizers needed a simple website that they could update and track the weeks events. I created a custom Wordpress theme listing events on the homepage sorted by the day of the week. Each event corresponded to a page quickly and easily created on the Wordpress back-end."
	}
</script>
