<div class="container">
	<div class="project-title">
		<h2>Skylens</h2>
		<h4>UX Design - 2020</h4>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/Skylens/HomePage2020.png" class="image-responsive" />
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p>
				Launching in 2020 Skylens is a spin-off company from <a href="https://www.superbrightleds.com/" target="_blank">SuperBrightLEDs.com</a> which was created in order to provide an enhanced shopping experience for our highly customizable and artistic backlit panel lights.
			</p>

			<p>
				With backlit artwork panels having so many variations in panel size, mounting type, and artwork style it has always been difficult to feature them with the presence and customization variations they deserve within the existing scaffolding of SuperBrightLEDS.com. It was for this reason that the decision was made to spin these products off into their own website and brand, allowing them to really stand on their own.
			</p>

			<p>
				The entire project was lead by our product marketing lead, graphic design lead, and myself with development support from a contracted third-party developer who built the site on the <a href="https://magento.com/" target="_blank">Magento 2 platform.</a>
			</p>

			<p>
				My personal areas of involvement were data architecture and management, site navigation and structure, and assistance with designing select interactive elements and wireframing certain pages. Our overall <a href="/page/SuperBrightLEDs">project development process was similar to the one we use for most other projects at SuperBrightLEDs.com</a> with slight variations due to this being a completely new site as opposed to changes or additions to an existing platform.
			</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
				<img src="<?=$cdn_url ?>/builtWith/Magento.png" class="img-responsive">
		</div>
	</div>
</div>

<div class="expanded-work">
	<video loop muted autoplay class="work-column left">
	  <source src="<?=$cdn_url ?>/Skylens/artwork-widget.mp4" type="video/mp4">
	  Your browser does not support the video tag.
	</video>
	<img src="<?=$cdn_url ?>/Skylens/SinglePanel.png" class="work-column right" />
	<img src="<?=$cdn_url ?>/Skylens/BrowseArt.png" class="work-column left" />
	
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Aligned Studio LLC.",
		"page_description" : "Aligned Studio LLC was founded by Micki Wehmeier and Randy Winzen as a full service architecture and interior design firm. Their website design was completed by Jack McEachern in 2016 and revised in 2019."
	}
</script>
