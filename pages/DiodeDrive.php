<div class="container">
	<div class="project-title">
		<h2>DiodeDrive</h2>
		<h4>Web Development & Design - 2020</h4>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/DiodeDrive/HomePage2020.png" class="image-responsive" />
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p>
				In my time at <a href="https://www.superbrightleds.com/" target="_blank">SuperBrightLEDs.com</a> we've created a number of our own product lines specific to certain lighting applications. The goal of these lines is to better illustrate to customers how a grouping of products interface with each other, or for easy comparison between highly similar products with slightly different feature sets.
			</p>

			<p>
				In the case of our DiodeDrive series we wanted to unify all of our power supplies under one branding guideline that could be easily speced against other third-party power supply manufacturers.
			</p>

			<p>
				This led to the creation of <a href="https://diodedrive.com/" target="_blank">DiodeDrive.com</a>. A simple website built on the <a href="https://reactjs.org/">React framework</a> designed to act as a catalog and branded portal to all of our first-party power supplies sold by <a href="https://www.superbrightleds.com/" target="_blank">SuperBrightLEDs.com</a>.
			</p>

			<p>
				The project was led jointly by our product marketing lead, graphic design lead, and myself. The site was primarily designed by our graphic designer with some minor contributions from me. Development was lead by me with contributions and support from our graphic design lead.
			</p>

			<p>
				The site employs a number of npm packages to easily facilitate data updates, animation events, and is fully responsive for mobile and devices of all shapes and sizes.
			</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
			<img src="<?=$cdn_url ?>/builtWith/React.png" class="built-with-icon">
		</div>
	</div>
</div>

<div class="expanded-work">
	<video loop muted autoplay class="work-column left">
	  <source src="<?=$cdn_url ?>/DiodeDrive/PDP-video.mp4" type="video/mp4">
	  Your browser does not support the video tag.
	</video>
	<img src="<?=$cdn_url ?>/DiodeDrive/Moreinfo-Page-2.png" class="work-column right">
	<img src="<?=$cdn_url ?>/DiodeDrive/Moreinfo-Page-1.png" class="work-column left">
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "DiodeDrive",
		"page_description" : "Architextures LLC is an architecture company in Webster Groves, Missouri. Jack McEachern completed a website redesign while working as an intern in the summer of 2014. He also assisted in producing new print collateral material."
	}
</script>
