<div class="container">
	<div class="project-title">
		<h2>Aligned Studio</h2>
		<h4>Wordpress Theming - 2019</h4>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/AlignedStudio/" class="image-responsive" />
</div>


<div class="container" id="content">
	<div class="project_gallery aligned-black" id="topGallery">
		<img src="<?=$cdn_url ?>/AlignedStudio/HomePage2020.png" data-main="">
		<video loop muted autoplay>
		  <source src="<?=$cdn_url ?>/AlignedStudio/AlignedStudio.mp4" type="video/mp4">
		  Your browser does not support the video tag.
		</video>
		<img src="<?=$cdn_url ?>/AlignedStudio/WorkPage-BloomCafe.png" data-main="">
	</div>

	<div id="imageSlider">
		<div id="sliderFill"></div>
	</div>

	<div class="project">
		<h2>Aligned Studio - 2019</h2>
	</div>

	<div class="project-details-container">
		<div class="project-description">
			<h5>About:</h5>
			<p>Micki Wehmeier and Randy Winzen founded <a href="https://aligned-studio.com/" target="_blank">Aligned Studio</a> in August of 2016 as a full service architecture and interior design firm performing work locally in St. Louis as well as nationally.</p>

			<p>Architecture and interior design being primarily a visual medium Aligned Studio needed to really make their photography standout and really draw in the attention of potential clients. For this reason simple fonts with little splashes of color to match their logo were choosen. This allows everything to be cohesive with a distinct, consistent look while focusing the most attention towards the large photography pieces.</p>

			<p>The homepage gets a work sample right up front underneath their logo, and the sites primary navigation menu.</p>

			<p>Each project was given it's own information page which can be navigated to from the work gallery linked in the top right navigation.</p>
			
			<p>Each project has a slider containing relevant project photos and renderings. The slider can be controlled through clicking and dragging, or clicking indivigual photos beneath the currently viewed slide. Call back functionality was added for certain projects to allow for the project description to be updated to provide information about the slide currently being viewed.</p>

			<p>Aligned Studio's entire website, including the image sliders fully supports mobile, and touch screen browsing.</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
			<img src="<?=$cdn_url ?>/builtWith/WordpressLogo.png" class="img-responsive">
		</div>
	</div>
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Aligned Studio LLC.",
		"page_description" : "Aligned Studio LLC was founded by Micki Wehmeier and Randy Winzen as a full service architecture and interior design firm. Their website design was completed by Jack McEachern in 2016 and revised in 2019."
	}
</script>
