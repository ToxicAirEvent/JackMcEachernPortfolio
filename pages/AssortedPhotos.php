<div class="container">
	<div class="project-title">
		<h2>Assorted Photos</h2>
		<h3>Photography - 2016</h3>
	</div>

	<div class="project-details-container breathing-room">
		<h5>About:</h5>
		<p>This is a collection of my work that I have thoroughly enjoyed doing. I hope you enjoy looking at my work.</p>
	</div>
</div>

<div class="expanded-work" id="content">
	<?php 
	$images = [
		"13588298405_ea35f1c411_o_compressed.jpg",
		"13789752343_2f83cdf9c3_o_compressed.jpg",
		"13789758223_3ebd62c2a2_o_compressed.jpg",
		"13790077794_a82031f17b_o_compressed.jpg",
		"13953802837_ffcb0b92e0_o_compressed.jpg",
		"13789749223_2af31d038f_k_compressed.jpg",
		"Clayton-and-Forest-Park-2019_UPDATED-5871.jpg",
		"Clayton-and-Forest-Park-2019-5939.jpg",
		"Clayton-and-Forest-Park-2019-5879.jpg",
		"14117317266_934483a323_o_compressed.jpg",
		"Outdoors-September-2014-9579_compressed.jpg",
		"Sculpture-Park-July-2016-3870_compressed_compressed.jpg",
		"Some-Bird-Thing-_compressed.jpg",
		"Through-The-Tree-_compressed.jpg",
		"Famed-outdoors_tree-line_compressed.jpg",
		"Gavin-Pole-Vaulting_Photo-Filtered_compressed.jpg",
		"Playing-with-effects-filters-_compressed.jpg",
		"2019-Air-Show-6264_sharable.jpg",
		"2019-Air-Show-6241_sharable.jpg",
		"2019-Air-Show-6112_sharable.jpg",
		"2019-Air-Show-6110_sharable.jpg",
		"Phil-And-Peyon-Wedding-4531_compressed.jpg",
		"14140733164_df327c5699_o_compressed.jpg",
		"14140526575_53bc973274_o_compressed.jpg",
		"14140522585_cfc6c7fbff_o_compressed.jpg",
		"14160583683_f446024d6d_o_compressed.jpg",
		"Jack-McEacher_FINAL-15-Portraits-0343_compressed.jpg",
		"Jack-McEacher_FINAL-15-Portraits-0362_compressed.jpg",
		"Jack-McEacher_FINAL-15-Portraits-0368_compressed.jpg",
		"Jack-McEacher_FINAL-15-Portraits-0375_compressed.jpg",
		"Jack-McEacher_FINAL-15-Portraits-0386_compressed.jpg",
		"Jack-McEacher_FINAL-15-Portraits-0399_compressed.jpg",
		"Jack-McEacher_FINAL-15-Portraits-0405_compressed.jpg",
		"Jack-McEacher_FINAL-15-Portraits-0412_compressed.jpg",
		"Jack-McEachern_Final-First-5-0110_compressed.jpg",
		"Jack-McEachern_Final-First-5-0221_compressed.jpg",
		"Jack-McEachern_Final-First-5-0272_compressed.jpg",
		"Jack-McEachern_Final-First-5-0319_compressed.jpg",
		"A Marathon High Five.jpg",
		"13588309963_a336e425c5_o_compressed.jpg",
		"13588314233_1e89ea614e_o_compressed.jpg",
		"13588460453_ef550fd7bf_o_compressed.jpg",
		"14140697204_8bcfd48f01_o_compressed.jpg",
		"14117288806_40ef23e0e0_o_compressed.jpg",
		"14117278206_ce6aa39d5b_o_compressed.jpg",
		"14117281346_f4e6a14ca1_o_compressed.jpg",
		"14117279896_fab523ab40_o_compressed.jpg",
	];
	
	foreach($images as $image): ?>
		<img src="<?=$cdn_url ?>/AssortedPhotos/<?= $image ?>" class="image-responsive scale_height project-images">
	<?php endforeach; ?>
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Assorted Photography",
		"page_description" : "Jack McEachern is a hobbiest photographer with a wide array of interest. View an assorted collection of his photos taken over the course of the last few years."
	}
</script>
