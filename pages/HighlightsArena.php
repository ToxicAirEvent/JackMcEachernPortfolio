<div class="container">
	<div class="project-title">
		<h2>Highlights Arena</h2>
		<h4>Web Development - 2017 to Present</h4>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/HighlightsArena/HomePage2020.png" class="image-responsive" />
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p><a href="https://highlightsarena.com/" target="_blank">Highlights Arena</a> is another personal project I built using the <a href="https://laravel.com/" target="_blank">Laravel framework.</a></p>
			<p>In some ways it started as a precursor to <a href="/page/ArmchairAnalyst">Armchair Analyst.</a> Around this time I was looking for something to do with sports data, photography, or videography. Which spawned the idea of a way to collect and web clip cool highlights from around the web.</p>
			<p>In the end this idea was abandonded since highlights are already so well kept on their respective services. However I still liked the idea of having a web clipper service for inspiration photography, graphic design, or just funny, unique, and interesting photos and videos.</p>
			<p>With the code already written to support embeding from various popular sites such as Twitter, Imgur, Gfycat, Instagram, and more it was an easy content shift to make. Simply just post whatever I wanted as opposed to sports related things.</p>
			<p>Wanting to make the posting process easier I added a <a href="https://gitlab.com/ToxicAirEvent/highlights-arena-web-clipper---chrome-extension" target="_blank">Chrome Extension</a> which wheck clicked detects the domain of the content, and then generates an embed request on Highlights Arena. To make posting secure each user is given a unique API key that is tied to their account. They can embed this API key in their extension which allows them to create post.</p>
			<p>While <a href="https://highlightsarena.com/" target="_blank">Highlights Arena</a> is still operational it is invite only for only myself and a few of my friends. However the entire project is <a href="https://gitlab.com/ToxicAirEvent/TheSportsReport" target="_blank">available on my Gitlab.</a> As is the accompanying <a href="https://gitlab.com/ToxicAirEvent/highlights-arena-web-clipper---chrome-extension" target="_blank">Chrome Extension</a> for anyone who would like to start their own personal web clipping service.</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
			<img src="<?=$cdn_url ?>/builtWith/laravel.png" class="built-with-icon">
		</div>
	</div>
</div>

<div class="expanded-work">
	<video loop muted autoplay class="work-column left">
	  <source src="<?=$cdn_url ?>/HighlightsArena/HighlightsArenaWebClipper.mp4" type="video/mp4">
	  Your browser does not support the video tag.
	</video>
	<img src="<?=$cdn_url ?>/HighlightsArena/ViewingArtTag.png" class="work-column right" />
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Highlights Arena",
		"page_description" : "Highlights Arena is an ongoing development project by Jack McEachern with content contributation by himself and his friends. The goal of the website is to share interesting opinions on sports and athletics as well as collect the best highlights from around the web."
	}
</script>
