<div class="container">
	<div class="project-title">
		<h2>Image Mashups</h2>
		<h3>Photography - 2016</h3>
	</div>

	<div class="project-details-container breathing-room">
		<h5>About:</h5>
		<p>A series of digital photographs that were edited, printed, cut up, drawn on, and reassembled for scanning.</p>
	</div>
</div>


<div class="expanded-work" id="content">
	<?php 
	$images = [
	"Downtown-cape.jpg",
	"Grifiti-HEPA.jpg",
	"Grifiti-SMILE.jpg",
	"Hand-Stop-Sign.jpg",
	"Stop-Sign-Stop-Light.jpg",
	];
	
	foreach($images as $image): ?>
		<img src="<?=$cdn_url ?>/mashups/<?= $image ?>" class="image-responsive scale_height project-images">
	<?php endforeach; ?>
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Image Mashups",
		"page_description" : "A set of photographs by Jack McEachern. Each photo is a composite of a series of other photos to form one entirely new photo. Click to view and learn more."
	}
</script>