<div class="container">
	<div class="project-title">
		<h2>Armchair Analyst</h2>
		<h4>Web Development & Design - 2018 to Present</h4>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/ArmchairAnalyst/ArmchairAnalystHome2020.png" class="image-responsive" />
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p><a href="https://armchairanalyst.net/" target="_blank">Armchair Analyst</a> was a personal project I started in 2018 as a way for people to manage and participate in many different college, and pro football survivor pools.</p>

			<p>Pro football survivor pools have been around for a long time but are usually managed through email list, or limited other services online which can prove to be costly. I wanted to make something that was free, easy to use, and added a twist with college football since that is what I most enjoy.</p>

			<p>Everything was built from the ground up by me using the <a href="https://laravel.com/" target="_blank">Laravel framework for php</a>, and features integrations with other APIs such as <a href="https://www.mailgun.com/" target="_blank">Mailgun</a> for email notifications of pick status, <a href="https://aws.amazon.com/s3/" target="_blank">AWS S3</a> for image upload, and as of 2019 <a href="https://stripe.com/" target="_blank">Stripe</a> for mulligan purchases.</p>

			<p>Working on Armchair Analyst has been a personal project in my free time that has taught me a lot about web development and design, both on the front end and backend. It's still active over at <a href="https://armchairanalyst.net/" target="_blank">armchairanalyst.net</a> and you can view selected screenshots, and marketing material created for it below.</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
				<img src="<?=$cdn_url ?>/builtWith/laravel.png" class="built-with-icon">
				<img src="<?=$cdn_url ?>/builtWith/mailgun.png" class="built-with-icon">
				<img src="<?=$cdn_url ?>/builtWith/stripe.png" class="built-with-icon">
		</div>
	</div>
</div>


<div class="expanded-work">
	<img src="<?=$cdn_url ?>/ArmchairAnalyst/AllPoolsPage.png" class="work-column left" />
	<video loop muted autoplay class="work-column right">
	  <source src="<?=$cdn_url ?>/ArmchairAnalyst/ArmchairAnalystPickSelect.mp4" type="video/mp4">
	  Your browser does not support the video tag.
	</video>
	<img src="<?=$cdn_url ?>/ArmchairAnalyst/Single-Pool-Page.png" class="work-column left" />
	<img src="<?=$cdn_url ?>/ArmchairAnalyst/Center-Top-Line-Logo.png" class="work-column right" />
	<img src="<?=$cdn_url ?>/ArmchairAnalyst/Leaderboard.png" class="work-column left" />
	<img src="<?=$cdn_url ?>/ArmchairAnalyst/Half_Page.png" class="work-column right" />
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Armchair Analyst.",
		"page_description" : "A professional and college football survivor pool pick'em challenge created by Jack McEachern."
	}
</script>
