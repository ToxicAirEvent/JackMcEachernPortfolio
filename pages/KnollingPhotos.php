<div class="container">
	<div class="project-title">
		<h2>Knolling Photos</h2>
		<h3>Photography - 2016</h3>
	</div>

	<div class="project-details-container breathing-room">
		<h5>About:</h5>
		<p>A series of photo grids originally inspired by the style of <a href="https://www.artgallery.nsw.gov.au/collection/works/430.2008.a-ii/" target="_blank">Edward Ruscha <em>Thirtyfour parking lots in Los Angeles</em> series.</a></p>
		<p>I liked the idea of applying this style to other places and things.  In this format, each photograph can stand on its own or <a href="<?=$site_url ?>/img/KnollingPhotos/Ed-Ruscha-Parking-Lots.jpg" target="_blank">the collection of photographs can tell a larger story.</a></p>
	</div>
</div>

<div class="expanded-work" id="content">
	<?php 
	$images = [
	"POSTERS-Photo-Grid-CHURCHES.jpg",
	"POSTERS-Photo-Grid-CLOTHING.jpg",
	"POSTERS-Photo-Grid-Houses-and-Abandoned-Places.jpg",
	"POSTERS-Photo-Grid-Poles-And-Lines.jpg",
	"POSTERS-Photo-Grid-Rock-Paper-scissors.jpg",
	];
	
	foreach($images as $image): ?>
		<img src="<?=$cdn_url ?>/KnollingPhotos/<?= $image ?>" class="image-responsive scale_height project-images">
	<?php endforeach; ?>
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Knolling Photos",
		"page_description" : "A series of photographs by Jack McEachern using a grid style layout to show the relationship between places and things. Learn more and view."
	}
</script>
