<div class="container">
	<div class="project-title">
		<h2>Orderly.news</h2>
		<h4>Web Development - 2017 to Present</h4>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/Orderly/HomePage2020.png" class="image-responsive" />
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p><a href="https://orderly.news/" target="_blank">Orderly.news</a> is a project I built using the <a href="https://laravel.com/" target="_blank">Laravel framework.</a> Originally I started this project under the moniker of <a href="https://gitlab.com/ToxicAirEvent/JustTheNews" target="_blank"><em>Just The News.</em></a> Which has been open sourced, but since retired.</p>
			<p>On thing I always liked about the early days of sites like Twitter is that the feed was order canonically with the newest items at the top, and the oldest items at the bottom. It made it extremely easy to follow the events of the day as they unfolded.</p>
			<p>Since then more and more services have moved to a algorithm, taste based feed methdology, and while I like and use many of these services I still wanted the ability to recount the days news, or series of events as they happened, from a wide variety of sources with differing viewpoints.</p>
			<p>From there <a href="https://orderly.news/" target="_blank">Orderly.News</a> was created. A pet project that I still periodically update with new features and actively functions to this day.</p>
			<p>For some background on functionality the stories are crawled using the <a href="https://github.com/simplepie/simplepie/" target="_blank">SimplePie library</a>, date and time information is largely parsed using <a href="https://carbon.nesbot.com/docs/" target="_blank">Carbon</a>, and some interactive elements are implemented using the <a href="https://vuejs.org/" target="_blank">Vue</a> framework. With core styling being handled by a customized version of <a href="https://getbootstrap.com/" target="_blank">Boostrap.</a></p>
			<p>Weather updates and forecast for your local area are fetched via your <a href="https://developer.mozilla.org/en-US/docs/Web/API/Geolocation_API" target="_blank">browsers Geolocation API</a> (if you allow it) and the data is provided by <a href="https://darksky.net/" target="_blank">Dark Sky.</a></p>
			<p>And for a final fun fact, the Orderly.News logo is made to resemble <a href="https://en.wikipedia.org/wiki/Morse_code" target="_blank">Morse Code.</a> Since Morse Code allows for the transmition of information, across many mediums, and the order of the information is highly important.</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
			<img src="<?=$cdn_url ?>/builtWith/laravel.png" class="built-with-icon">
			<img src="<?=$cdn_url ?>/builtWith/vuejs.svg" class="built-with-icon">
			<img src="<?=$cdn_url ?>/builtWith/darksky.png" class="built-with-icon">
		</div>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/Orderly/FollowedSources2020.png" class="work-column left" />
	<img src="<?=$cdn_url ?>/Orderly/PopularStories.png" class="work-column left" />
	<video loop muted autoplay class="work-column right">
	  <source src="<?=$cdn_url ?>/Orderly/OrderlyNewsSetFrontPage.mp4" type="video/mp4">
	  Your browser does not support the video tag.
	</video>
	<img src="<?=$cdn_url ?>/Orderly/Dashboard2020.png" class="work-column right" />
	<img src="<?=$cdn_url ?>/Orderly/YourStories2020.png" class="work-column right" />
	<video loop muted autoplay class="work-column left">
	  <source src="<?=$cdn_url ?>/Orderly/OrderlyPayWall.mp4" type="video/mp4">
	  Your browser does not support the video tag.
	</video>
	<img src="<?=$site_url ?>/img/OrderlyNewsIcon-Large.png" class="work-column right" />
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Orderly.news",
		"page_description" : "Orderly.news is a web development project by Jack McEachern. The function of the site is to show news in the order it happens from a variety of sources without algorithms."
	}
</script>
