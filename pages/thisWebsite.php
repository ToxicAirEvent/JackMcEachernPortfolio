<div class="container" id="content">
	<div class="project">
		<h2>This Portfolio</h2>
		<h3>Web Design - 2018 to Present.</h3>

		<p>When I wanted to update my portfolio I decided to build my own framework for it using php. I needed a platform that was lightweight and easy to modify and add new content to as I complete projects over time.</p>
		<p>There are many excellent frameworks out there but as a learning exercise I wanted to try and make my own basic one that could handle a few simple functions for page management and standardization of
		content elements pieces across multiple pages.</p>
		<p>As a result <a href="https://gitlab.com/ToxicAirEvent/JackMcEachernPortfolio" target="_blank">my portfolio framework was born.</a></p>
		<p>You can see it in action by simply clicking around this website and view the code base for it's functionality <a href="https://gitlab.com/ToxicAirEvent/JackMcEachernPortfolio" target="_blank">over on my gitlab.</a></p>
		<p>If you want to use the framework yourself feel free to clone it and update it for your own personal needs.</p>
	</div>
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "This Portfolio",
		"page_description" : "The website for my portfolio was designed using a custom built minimal PHP framework designed to quickly generate and display pages with static content."
	}
</script>