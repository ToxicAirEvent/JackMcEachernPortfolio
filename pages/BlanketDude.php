<div class="container">
	<div class="project-title">
		<h2>Blanket Dude</h2>
		<h3>Graphic Design - 2017</h3>
	</div>

	<div class="project-details-container breathing-room">
		<h5>About The Project:</h5>
		<p>In 2017, Blanket Dude hired me to create and redesign some splash images and promotional banners for <a href="https://www.blanketdude.com/" target="_blank"> the company website.</a></p>
	<p>The banner purpose was to act as a header of carousel images which would allow a variety of product categories to be displayed.  
	Multiple version of each banner was prepared for use across a number of locations.  
	The banners needed to be versatile enough for use on promotional material outside the website itself.</p>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/BlanketDude/BlanketDude-AmericanMade.jpg" class="image-responsive center-image ptn pbl" />
	<img src="<?=$cdn_url ?>/BlanketDude/BlanketDude-AmericanMilitary_3.jpg" class="image-responsive center-image pvl" />
	<img src="<?=$cdn_url ?>/BlanketDude/BlanketDude-Christmas_updated.jpg" class="image-responsive center-image pvl" />
	<img src="<?=$cdn_url ?>/BlanketDude/BlanketDude-Freemasons.jpg" class="image-responsive scale_height center-image pvl" />
	<img src="<?=$cdn_url ?>/BlanketDude/BlanketDude-MothersDay_Custom-Blankets.jpg" class="image-responsive center-image pvl" />
	<img src="<?=$cdn_url ?>/BlanketDude/BlanketDude-PoliceAndFIre_Enlarged-title.jpg" class="image-responsive center-image pvl" />
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Blanket Dude",
		"page_description" : "Blanket Dude is an independed ecommerce store specializing in custom and unique blankets and tote bags. Jack McEachern has assisted the company in graphic design and content optimization for their website as well as SEO optimizations."
	}
</script>