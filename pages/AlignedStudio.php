<div class="container">
	<div class="project-title">
		<h2>Aligned Studio</h2>
		<h4>Wordpress Theming - 2016</h4>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/AlignedStudio/HomePage2020.png" class="image-responsive" />
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p>Micki Wehmeier and Randy Winzen founded <a href="https://aligned-studio.com/" target="_blank">Aligned Studio</a> in August of 2016 as a full service architecture and interior design firm performing work for client all across the United States.</p>

			<p>Architecture and interior design being primarily a visual medium Aligned Studio needed to really make their photography standout and really draw in the attention of potential clients. For this reason simple fonts with little splashes of color to match their logo were choosen. This allows everything to have a cohesive style while focusing the most attention towards the large photography pieces.</p>
			
			<p>Each project has a slider containing relevant project photos and renderings. The slider can be controlled through clicking and dragging, or clicking indivigual photos beneath the currently viewed slide. Call back functionality was added for certain projects to allow for the project description to be updated to provide information about the slide currently being viewed.</p>

			<p>Aligned Studio's entire website, including the image sliders fully supports mobile, and touch screen browsing.</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
				<img src="<?=$cdn_url ?>/builtWith/WordpressLogo.png" class="img-responsive">
		</div>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/AlignedStudio/WorkPage2020.png" class="work-column left" />
	<img src="<?=$cdn_url ?>/AlignedStudio/WorkPage-BloomCafe2020.png" class="work-column left" />
	<video loop muted autoplay class="work-column right">
	  <source src="<?=$cdn_url ?>/AlignedStudio/AlignedStudio.mp4" type="video/mp4">
	  Your browser does not support the video tag.
	</video>
	<img src="<?=$cdn_url ?>/AlignedStudio/TeamPage2020.png" class="work-column right" />
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Aligned Studio LLC.",
		"page_description" : "Aligned Studio LLC was founded by Micki Wehmeier and Randy Winzen as a full service architecture and interior design firm. Their website design was completed by Jack McEachern in 2016 and revised in 2019."
	}
</script>
