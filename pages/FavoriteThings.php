<div class="container">
	<div class="project-title">
		<h2>Favorite Things</h2>
		<h4>Web Design &amp; Wordpress Theming - 2016</h4>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/FavoriteThings/HomePage2020.png" class="image-responsive" />
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p>Negative Space is a <a href="https://gitlab.com/ToxicAirEvent/NegativeSpace-WP-Theme" target="_blank">wordpress theme I created and published to Gitlab.</a> I use it for my personal blog that I sparsely update with cool things I find online.</p>
			<p>In the event anyone else wanted to use it I recently made it open source for anyone to use for noncommercial purposes.</p>
			<p>The theme has a number of little extras that allow you to customize the blog to best suit your needs. A custom header image can be set, or multiple images can be uploaded for one to be chosen randomly every time the page is loaded.
			Google Analytics tracking can easily be added to any blog using the analytics.php file located in the themes root directory.</p>
			<p>Customization of the sites navigation menu, and sidebar widgets are fully supported via the Wordpress admin/management console.</p>
			<p>If you'd like to see the theme in action you can find it on my <a href="https://favoritethings.jackmceachern.com/" target="_blank">favoritethings.jackmceachern.com sub-domain.</a></p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
			<img src="<?=$cdn_url ?>/builtWith/WordpressLogo.png" class="img-responsive">
		</div>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/FavoriteThings/SinglePost2020.png" class="work-column left">
	<img src="<?=$cdn_url ?>/FavoriteThings/BrowsingCategory.png" class="work-column right">
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Negative Space Wordpress Theme",
		"page_description" : "Negative Space is a wordpress theme by Jack McEachern used on his personal blog Favorite Things. It tries to strike an artful yet minimal look and is designed to showcase photography and other images."
	}
</script>
