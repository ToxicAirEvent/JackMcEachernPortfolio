<div class="container">
	<div class="project-title">
		<h2>Super Bright LEDs</h2>
		<h4>User Experience - 2016 to Present</h4>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/SBL/SuperBrightLEDsCover.jpg" class="image-responsive" />
</div>

<div class="container container deep-breath">
	<div class="project-details-container">
		<h5>Background:</h5>
		<p>
			I began working at SuperBrightLEDs.com in November of 2016 as a member of the web content department. My position was initially tasked with front end code maintenance, and product data updates to category landing pages and individual product data pages.
		</p>

		<p>As the company has evolved and grown so has my position.</p>

		<p>Today I work with our lead graphic designer to oversee a small team responsible for all user experience, design, navigation, and SEO improvements to the overall shopping experience of the website. We work closely with, and at times alongside the product marketing team to ensure that the information presented to customers is easy to understand and technically accurate. Often times jointly working on projects involving paid advertising and written content revisions.</p>
	</div>
</div>

<div class="expanded-work deep-breath">
	<div class="tri-images">
		<img src="<?=$cdn_url ?>/SBL/SBL-Dividing-Images-1.jpg">
	</div>

	<div class="tri-images">
		<img src="<?=$cdn_url ?>/SBL/SBL-Dividing-Images-2.jpg" class="tri-image-margin-middle">
	</div>

	<div class="tri-images">
		<img src="<?=$cdn_url ?>/SBL/SBL-Dividing-Images-3.jpg" class="tri-image-margin-end">
	</div>
</div>

<div class="container deep-breath">
	<div class="project-details-container">
		<h3>The Process:</h3>
		<p>Every project presents a unique set of challenges, and requires creative problem solving to meet and solve those challenges.</p>
		<p>However in my time at SuperBrightLEDs we've worked to generate a process that is a guiding framework that serves as a rough outline for most projects we embark on. With the five main steps being data analysis, navigation and functionality mapping, design discussion and revision, implementation, and testing.</p>
	</div>
</div>

<div class="expanded-work">
	<div class="work-column left">
		<h5 class="mbn">Step 1</h5>
		<h4 class="mts">Data Analysis</h4>
		<p>In an effort to make impactful changes we always begin with a data driven analysis on the performance of various portions of our site to evaluate the key performance indicators of any given page, category, product line, or site feature.</p>

		<p>The proactive approach to performance analysis allows us to be forward looking in our project planning. Indentifying where things are under performing and where we shouldn't be messing with success.</p>
	</div>

	<div class="work-column right">
		<img src="<?=$cdn_url ?>/SBL/GraphSample1.png" class="left image-responsive" />
	</div>
</div>

<div class="expanded-work heavy-sigh">
	<div class="work-column right">
		<h5 class="mbn">Step 2</h5>
		<h4 class="mts">Navigation and Functionality Mapping</h4>
		<p>
			Once we've identified a problem spot on the website we take the time to map out the portion of the site as it currently exist in a flowchart.
		</p>

		<p>
			Then using that flowchart we begin to remap that portion of the site and its functionality. This allows us to understand in a clear visual view the category structre and overlap, and where a functionality is missing. Such as a guided shopping tool, rebate widget, a better filtering experience, or something totally new we'll need to dream up.
		</p>

		<p>
			Additionally at this stage we do a lot of competitor analysis to understand their website structures, features, functionality, and product nomenclature. This allows us to create shopping experiences inline with industry standards while adding what we feel are design and functionality improvements unique to our website and product lines.
		</p>
	</div>

	<div class="work-column left">
		<img src="<?=$cdn_url ?>/SBL/SBL-Flowchart-Example-Graphic.png" class="left image-responsive" />
	</div>
</div>

<div class="expanded-work heavy-sigh">
	<div class="work-column left">
		<h5 class="mbn">Step 3</h5>
		<h4 class="mts">Design discussion and revision</h4>
		
		<p>
			Once we've mapped out and decided on our desired functionality and design structure for a given project we move onto creating the actual content which will go into the website.
		</p>

		<p>
			Usually this is a series of conceptual designs, or wireframes (see right) created by our in-house graphic designer which are then presented to all relevant departments in order to facilitate discussion as to what is the best implementation of any given navigation structure, feature, or educational content for the website.
		</p>

		<p>
			Through multiple meetings and discussions these designs and wireframes are continually refined more and more until we reach a point where we feel like the overall quality and functionality has reached a point that it is viable for development.
		</p>

		<p>
			As I said, this portion of the process is carried out primarily by our in-house graphic designer, however over the last few years I have worked to become more involved with some of the early parts of this step such as wire framing and building functional prototypes in softwares like Adobe XD. With her becoming more involved in the data and development gathering steps as well.
		</p>

		<p>
			This has lead us to a position, and environment that is more collaborative with projects being completed quicker because there are less roadblocks at any given stage where only one person can handle the task effectively.
		</p>
	</div>

	<div class="work-column right">
		<img src="<?=$cdn_url ?>/SBL/WireFrameExample.png" class="left image-responsive" />
	</div>
</div>

<div class="expanded-work heavy-sigh">
	<div class="work-column right">
		<h5 class="mbn">Step 4</h5>
		<h4 class="mts">Implementation (Development)</h4>
		
		<p>
			Once we've got our designs to a point where we feel like they could be pushed live for public consumption it is primarily my responsibility to oversee the development of these pages. Doing large chunks of the development myself in most cases; with support from our in-house graphic designer, and development team.
		</p>

		<p>
			The website was built primarily on the <a href="https://laravel.com/" target="_blank">Laravel framework</a> with the use of many other libraries, and third-party APIs to implement many of the sites necessary functionalities. With the backbone of our front-end styling relying on a highly customized version of <a href="https://getbootstrap.com/" target="_blank">Bootstrap.</a>
		</p>

		<p>
			As of summer 2019 we've begun the process of migrating our platform over to <a href="https://magento.com/" target="_blank">Magento 2</a> which is still ongoing and is something my team has been working closely with our development team to accomplish.
		</p>
	</div>

	<video loop muted autoplay class="work-column left">
  		<source src="<?=$cdn_url ?>/SBL/vehicle-landing-page-cropped.mp4" type="video/mp4">
  		Your browser does not support the video tag.
	</video>
</div>

<div class="expanded-work heavy-sigh">
	<div class="work-column left">
		<h5 class="mbn">Step 5</h5>
		<h4 class="mts">Testing</h4>
		
		<p>
			Using a data driven and iterative design based approach we are able to increase our accuracy in making site revisions that have meaningful impact right out of the gate. However there are still times where we are internally conflicted as to what the best way forward is, or we just straight up get it wrong. For this reason we lean heavily on A/B testing through tools like <a href="https://marketingplatform.google.com/about/optimize/" target="_blank">Google Optimize</a>, and analytic data review with things like <a href="https://marketingplatform.google.com/about/analytics/" target="_blank">Google Analytics.</a>
		</p>

		<p>
			A/B or multivariate testing provides an upside in that they act as great tiebreakers for things we couldn't settle on internally as to what was the best way forward. In cases where an A/B test is not clearly defined or easily implemented we use the analytic review method to check in on performance a given number of days after the changes were implemented. This allows us to see if changes are working or not, and tells us if we have to go back to the drawing board.
		</p>
	</div>

	<div class="work-column right">
		<img src="<?=$cdn_url ?>/SBL/UBF-A-B-Test.png" class="left image-responsive" />
	</div>
</div>

<div class="container deep-breath">
	<div class="project-details-container">
		<h3>Revisions Using This Method:</h3>
		<p>Below are some screenshots of category landing pages that were redesigned using the method outlined above.</p>

		<p>While the category landing pages were the most signficant visual changes made they translate into being a portal for larger category structure, and user experience revisions within the site. Additionally this method of design development has been applied to changes on product data pages, informational pages, and the sites top level navigation bar.</p>

		<p>Clicking on any category landing page image will take you to the live version on <a href="https://www.superbrightleds.com/" target="_blank">SuperBrightLEDs.com</a> where you can explore the overall structure and site as a whole.</p>
	</div>
</div>

<div class="expanded-work breathing-room">
	<a href="https://www.superbrightleds.com/cat/led-light-bulbs-universal-finder/" target="_blank">
		<img src="<?=$cdn_url ?>/SBL/UBF.png" class="work-column left" />
	</a>

	<a href="https://www.superbrightleds.com/cat/vehicle-lights/" target="_blank">
		<img src="<?=$cdn_url ?>/SBL/vehicle.png" class="work-column right" />
	</a>

	<a href="https://www.superbrightleds.com/cat/led-strips-and-bars/" target="_blank">
		<img src="<?=$cdn_url ?>/SBL/strip-lights.png" class="work-column left" />
	</a>

	<a href="https://www.superbrightleds.com/cat/off-road-lights/" target="_blank">
		<img src="<?=$cdn_url ?>/SBL/Off-Road.png" class="work-column right" />
	</a>

	<a href="https://www.superbrightleds.com/cat/industrial-led-lighting/" target="_blank">
		<img src="<?=$cdn_url ?>/SBL/industrial.png" class="work-column left" />
	</a>
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Super Bright LEDs",
		"page_description" : "Since 2016 I have been a member of the web content and eCommerce teams at SuperBrightLEDs.com working to improve the overall shopping experience of the website."
	}
</script>
