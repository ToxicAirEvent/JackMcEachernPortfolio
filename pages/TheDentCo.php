<div class="container">
	<div class="project-title">
		<h2>The Dent Co</h2>
		<h3>Web Design - 2013</h3>
	</div>
</div>

<div class="expanded-work">
	<video loop muted autoplay class="img-responsive">
	  <source src="<?=$cdn_url ?>/TheDentCo/TheDentCo-Scroll.mp4" type="video/mp4">
	  Your browser does not support the video tag.
	</video>
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p>
				<a href="<?=$site_url ?>/TheDentCo/" target="_blank" title="See an archieved copy of The Dent Co's website in a new tab.">The Dent Co</a> is a automotive repair company in St. Louis Missouri which specializes in paintless dent repair. The company itself is rather small and highly mobile since a majority of their work involves traveling all over the country to repair vehicles damaged in storms.
			</p>

			<p>
				With their work being highly diverse and mobile they wanted a website that would act primarily as a contact point and brochure for their work and detail what paintless dent repair is.
			</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
			<div class="builtWithSimple">
				<img src="<?=$cdn_url ?>/builtWith/HTML.png" class="built-with-icon"><p>HTML</p>
			</div>

			<div class="builtWithSimple">
				<img src="<?=$cdn_url ?>/builtWith/CSS.png" class="built-with-icon"><p>CSS</p>
			</div>

			<div class="builtWithSimple">
				<img src="<?=$cdn_url ?>/builtWith/Javascript.png" class="built-with-icon"><p>Javascript</p>
			</div>
		</div>
	</div>
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "The Dent Co.",
		"page_description" : "A simple brochure style website for The Dent Co. A St. Louis based paintless dent repair company."
	}
</script>