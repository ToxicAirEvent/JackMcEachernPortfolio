<div class="container" id="about">
	<div class="headshot">
		<img src="<?=$cdn_url ?>/Me/Me1.jpg" id="isme" class="image-responsive" />
	</div>

	<script>
		window.addEventListener('load', function () {
		  const imagesOfMe = [
		  	"Me1.jpg",
		  	"Me2.jpg",
		  	"Me3.jpg",
		  	"Me4.jpg",
		  	"Me5.jpg",
		  	"Me6.jpg",
		  	"Me7.jpg",
		  	"Me8.jpg",
		  	"Me9.jpg",
		  	"Me10.jpg",
		  	"Me11.jpg",
		  	"Me12.jpg"
		  ];

		  const recents = ["Me1.jpg","Me1.jpg","Me1.jpg"];

		  var pullMe = setInterval(pullRandom, 2345);

		  function pullRandom(){
		  	let randomMe = imagesOfMe[Math.floor(Math.random() * imagesOfMe.length)];

		  	if(!recents.includes(randomMe)){
		  		let pathToNewMe = 'https://s3.us-east-2.amazonaws.com/thecooltable/img/Me/' + randomMe;
		  		$('#isme').attr('src',pathToNewMe);

		  		recents.shift();
		  		recents.push(randomMe);
		  	}else{
		  		pullRandom();
		  	}
		  }
		  
		})
	</script>
	
	<div class="summary">
		<h2 id="summary">About</h2>
		<p>Hi, I’m Jack McEachern, a St. Louis, Missouri based user experience designer and web developer.</p>
		
		<p>I received a B.S. in Computer Graphics and Multimedia Design from Southeast Missouri State University in August, 2016.</p>
		
		<p>
			I have worked at <a href="" target="_blank">SuperBrightLEDs.com</a> since November of 2016. 
			Starting out I worked in the web content department doing general front-end maintenance.
			Over time my roll has expanded to also include user experience design, search engine optimization, front-end development, and data architecture.
			Since October of 2018 I have overseen the eCommerce department alongside our lead graphic designer where we are tasked with all aspects of improving the sites overall shopping experience.
		</p>

		<p>On top of working at Super Bright LEDs I have worked as a freelance web designer and developer since high school where I have completed a number of projects for a wide variety of clients.</p>
		
		<p>I also maintain a number of pet projects for personal enjoyment and to build my development and design skills.</p>
		
		<h2 id="contact">Contact</h2>
		<h3>Phone: (314) 686-3598</h3>
		<h3>Email: <a href="mailto:jackemceachern@gmail.com">jackemceachern[at]gmail.com</a></h3>
	</div>
</div>

<div class="container" id="work">
	<div class="item" id="item25" data-vibrant-type="DarkVibrant">
		<a href="<?=$site_url ?>/index.php?page=SuperBrightLEDs">
			<img src="<?=$site_url ?>/img/SBL_Thumb.png" alt="Explanation of my work at SuperBrightLEDs" />
			<h1>SuperBrightLEDs.com</h1>
			<p>UX Design - 2016 to Present.</p>
		</a>
	</div>

	<div class="item" id="item24" data-vibrant-type="Vibrant">
		<a href="<?=$site_url ?>/index.php?page=DiodeDrive">
			<img src="<?=$site_url ?>/img/DiodeDrive_thumb.png" alt="About the development of DiodeDrive" />
			<h1>DiodeDrive</h1>
			<p>Web Development - 2020.</p>
		</a>
	</div>

	<div class="item" id="item26" data-vibrant-type="Vibrant">
		<a href="<?=$site_url ?>/index.php?page=Skylens">
			<img src="<?=$site_url ?>/img/Skylens_thumb.png" alt="About the development of DiodeDrive" />
			<h1>Skylens</h1>
			<p>UX Design - 2020.</p>
		</a>
	</div>

	<div class="item" id="item23" data-vibrant-type="LightVibrant">
		<a href="<?=$site_url ?>/index.php?page=BolandEnergy">
			<img src="<?=$site_url ?>/img/Boland-Energy_thumb.png" alt="Boland Energy Website Design" />
			<h1>Boland Energy</h1>
			<p>Web Design, &amp; Wordpress Theming - 2019.</p>
		</a>
	</div>

	<div class="item" id="item22" data-vibrant-type="DarkVibrant">
		<a href="<?=$site_url ?>/index.php?page=AlignedStudio">
			<img src="<?=$site_url ?>/img/Aligned-Studio-Listing-Swatch.png" alt="Aligned Studio - Interior Design and Architecture" />
			<h1>Aligned Studio</h1>
			<p>Web Design, &amp; Wordpress Theming - 2019.</p>
		</a>
	</div>

	<div class="item" id="item0" data-vibrant-type="LightVibrant">
		<a href="<?=$site_url ?>/index.php?page=ArmchairAnalyst">
			<img src="<?=$site_url ?>/img/ArmchairAnalystSwatch.png" alt="Armchair Analyst - College and Pro Football Survivor Pools" />
			<h1>Armchair Analyst</h1>
			<p>Web Development - 2018.</p>
		</a>
	</div>

	<div class="item" id="item19" data-vibrant-type="LightVibrant">
		<a href="<?=$site_url ?>/index.php?page=OrderlyNews">
			<img src="<?=$site_url ?>/img/OrderlyNewsIcon-Large.png" alt="An RSS news crawler and organizer." />
			<h1>Orderly.news</h1>
			<p>Web Development - 2017 to Present.</p>
		</a>
	</div>

	<div class="item" id="item20" data-vibrant-type="Muted">
		<a href="<?=$site_url ?>/index.php?page=HighlightsArena">
			<img src="<?=$site_url ?>/img/HighlightsArena_thumb.png" alt="A sports highlights archiving service." />
			<h1>Highlights Arena</h1>
			<p>Web Development - 2017 to Present.</p>
		</a>
	</div>

	<div class="item" id="item7" data-vibrant-type="DarkVibrant">
		<a href="<?=$site_url ?>/index.php?page=BlanketDude">
			<img src="<?=$site_url ?>/img/BlanketDude_thumb.png" alt="The Blanket Dude is an ecommerce company located in St. Louis." />
			<h1>Blanket Dude</h1>
			<p>Graphic Design - 2017.</p>
		</a>
	</div>

	<div class="item" id="item12" data-vibrant-type="LightVibrant">
		<a href="<?=$site_url ?>/index.php?page=HonestJunkRemoval">
			<img src="<?=$site_url ?>/img/HonestJunk_Thumb.png" alt="A wordpress theme created by Jack McEachern for Honest Junk Removal." />
			<h1>Honest Junk Removal</h1>
			<p>Web Design, &amp; Wordpress Theming - 2017.</p>
		</a>
	</div>

	<div class="item" id="item17" data-vibrant-type="skip">
		<a href="<?=$site_url ?>/index.php?page=FavoriteThings">
			<img src="<?=$site_url ?>/img/FavoriteThings_Wordpress_thumb.png" alt="A wordpress theme by Jack McEachern for anyone who wants to use it." />
			<h1>Favorite Things</h1>
			<p>Web Design &amp; Wordpress Theming - 2016.</p>
		</a>
	</div>

	<div class="item" id="item15" data-vibrant-type="LightVibrant">
		<a href="<?=$site_url ?>/index.php?page=SprogWeek">
			<img src="<?=$site_url ?>/img/SprogWeek_Thumb.png" alt="A wordpress theme developed by Jack McEachern for Sprog Week." />
			<h1>Sprog Week</h1>
			<p>Web Design &amp; Wordpress Theming - 2016.</p>
		</a>
	</div>

	<div class="item" id="item11" data-vibrant-type="skip">
		<a href="<?=$site_url ?>/index.php?page=AssortedGraphicDesign">
			<img src="<?=$site_url ?>/img/AssortedGraphicDesigns_Thumb.png" alt="Assorted graphic design projects by Jack McEachern." />
			<h1>Graphic, Logo, and Print Design</h1>
			<p>2015 to present.</p>
		</a>
	</div>

	<div class="item" id="item2" data-vibrant-type="Muted">
		<a href="<?=$site_url ?>/index.php?page=ImageMashups">
			<img src="<?=$site_url ?>/img/ImageMashups_thumb.png" alt="Digital photography image mashups" />
			<h1>Image Mashups</h1>
			<p>Photography - 2016.</p>
		</a>
	</div>

	<div class="item" id="item5" data-vibrant-type="skip">
		<a href="<?=$site_url ?>/index.php?page=FoodRun">
			<img src="<?=$site_url ?>/img/FoodRun_Thumb.jpg" alt="A stop motion animation." />
			<h1>Food Run</h1>
			<p>Video Production - 2016.</p>
		</a>
	</div>
	
	<div class="item" id="item13" data-vibrant-type="Muted">
		<a href="<?=$site_url ?>/index.php?page=KnollingPhotos">
			<img src="<?=$site_url ?>/img/KnollingGrids.jpg" alt="Photo grids" />
			<h1>Photo Grids</h1>
			<p>Photography - 2016.</p>
		</a>
	</div>

	<div class="item" id="item3" data-vibrant-type="LightVibrant">
		<a href="<?=$site_url ?>/index.php?page=MOTHistoryInPhotos">
			<img src="<?=$site_url ?>/img/MuseumSlideShow_thumb.jpg" alt="Photo slideshow produced for the Museum of Transportation." />
			<h1>Museum of Transportation History In Photos</h1>
			<p>Video and Photo Editing - 2016.</p>
		</a>
	</div>

	<div class="item" id="item4" data-vibrant-type="LightVibrant">
		<a href="<?=$site_url ?>/index.php?page=ArchitexturesWebsite">
			<img src="<?=$site_url ?>/img/Architextures_thumb.png" alt="Architextures website design in 2014." />
			<h1>Architextures</h1>
			<p>Web Design, Graphic Design, Photography - 2014.</p>
		</a>
	</div>

	<div class="item" id="item6" data-vibrant-type="LightVibrant">
		<a href="<?=$site_url ?>/index.php?page=DanielDwyerMusic">
			<img src="<?=$site_url ?>/img/DDMusic_thumb.png" alt="Danield Dwyer is a location St. Louis musician and I made his website for him." />
			<h1>Daniel Dwyer Music</h1>
			<p>Web Design - 2014.</p>
		</a>
	</div>

	<div class="item" id="item8" data-vibrant-type="LightVibrant">
		<a href="<?=$site_url ?>/index.php?page=AssortedPhotos">
			<img src="<?=$site_url ?>/img/AssortedPhotos_Thumb.png" alt="Assorted photography by Jack McEachern." />
			<h1>Assorted Photos</h1>
			<p>Photography - 2014 to &infin;.</p>
		</a>
	</div>

	<div class="item" id="item1" data-vibrant-type="LightVibrant">
		<a href="<?=$site_url ?>/index.php?page=TheDentCo">
			<img src="<?=$site_url ?>/img/TheDentCo-Thumb.png" alt="The Dent Co" />
			<h1>The Dent Co</h1>
			<p>Web Design - 2013.</p>
		</a>
	</div>

	<div class="item" id="item9" data-vibrant-type="LightVibrant">
		<a href="<?=$site_url ?>/index.php?page=nSKYELLC">
			<img src="<?=$site_url ?>/img/nSKYE_Thumb.png" alt="nSKYE is an grant writing company in St. Louis Missouri." />
			<h1>nSKYE llc</h1>
			<p>Web Design - 2012.</p>
		</a>
	</div>
	
	<div class="item" id="item18" data-vibrant-type="skip">
		<a href="<?=$site_url ?>/index.php?page=TheCoolBot">
			<img src="<?=$site_url ?>/img/TheCoolBot_Thumb.png" alt="A node.js development project for game server queries." />
			<h1>The Cool Bot</h1>
			<p>Node.js Development - 2017 &amp; 2018.</p>
		</a>
	</div>

	<div class="item" id="item10" data-vibrant-type="LightVibrant">
		<a href="<?=$site_url ?>/index.php?page=Animation">
			<img src="<?=$site_url ?>/img/Animation_Thumb.png" alt="Assorted animation by Jack McEachern made in Autodesk Maya." />
			<h1>Animations</h1>
			<p>Autodesk Maya - 2015, 2016, &amp; 2017.</p>
		</a>
	</div>

	<div class="item" id="item16" data-vibrant-type="LightMuted">
		<a href="<?=$site_url ?>/index.php?page=TheMagicSpell">
			<img src="<?=$site_url ?>/img/TheMagicSpell_thumb.jpg" alt="The Magic Spell is a childrens book by Terri McEachern." />
			<h1>The Magic Spell</h1>
			<p>Web Design - 2011.</p>
		</a>
	</div>

	<div class="item" id="item21" data-vibrant-type="LightVibrant">
		<a href="<?=$site_url ?>/index.php?page=thisWebsite">
			<img src="<?=$site_url ?>/img/JackMcEachernDesign_thumb.png" alt="A PHP framework created by Jack McEachern." />
			<h1>My Own Portfolio</h1>
			<p>Web Development - 2018 to Present.</p>
		</a>
	</div>
</div>