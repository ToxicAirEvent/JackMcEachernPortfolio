<div class="container">
	<div class="project-title">
		<h2>nSKYE llc</h2>
		<h3>Web Design - 2012</h3>
	</div>
</div>

<div class="expanded-work">
	<video loop muted autoplay class="img-responsive">
	  <source src="<?=$cdn_url ?>/nSKYE/nSkyeLLC-Scroll.mp4" type="video/mp4">
	  Your browser does not support the video tag.
	</video>
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p>
				<a href="https://nskyellc.com/" target="_blank">nSKYE LLC</a> a grant writing and project management company in St. Louis, Missouri, wanted a brief resume' website highlighting past projects.
			</p>

			<p>
				This was accomplished using a <a href="https://github.com/JoelBesada/scrollpath" target="_blank">jQuery plugin called <em>Scroll Path.</em></a>
			</p>

			<p>
				Scroll Path allows for an interesting and engaging way to present bite-sized pieces of information without being too overwhelming for perspective clients. The site was then customized with links placed at the bottom of the screen enabling users to advance to the slide or view pages with additional information.
			</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
			<div class="builtWithSimple">
				<img src="<?=$cdn_url ?>/builtWith/HTML.png" class="built-with-icon"><p>HTML</p>
			</div>

			<div class="builtWithSimple">
				<img src="<?=$cdn_url ?>/builtWith/Javascript.png" class="built-with-icon"><p>Javascript</p>
			</div>
		</div>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/nSKYE/nSkye-Full-resume.png" class="image-responsive">
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "nSKYE llc",
		"page_description" : "A simple website designed for nSKYE llc grant writing services. It uses a jquery scroll path plugin to showcase slides highlighting work done by the members of nSKYE llc. Learn more and view."
	}
</script>
