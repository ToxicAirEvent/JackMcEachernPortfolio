<div class="container">
	<div class="project-title">
		<h2>Food Run</h2>
		<h3>Video &amp; Photo Editing - 2016</h3>
	</div>

	<div class="project-details-container breathing-room">
		<h5>About:</h5>
		<p>A stop motion animation video made of the course of a few days in the kitchen of a house I once lived at in Cape Girardeau.</p>
		<p>Some minor photo corrections and touchups were done in Adobe Photoshop. Editing and sequencing was done using Adobe Premiere Pro.</p>
	</div>
</div>

<div class="expanded-work" id="content">
	<div class="youtube-video-container">
		<iframe width="1512" height="850" src="https://www.youtube.com/embed/Qw3o7L3IyR4?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	</div>
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Food Run",
		"page_description" : "Food Run is a stop motion animation by Jack McEachern. It was shot on a DSL camera with editing and sound being completed in Adobe Premier Pro. Learn more and watch."
	}
</script>
