<div class="container">
	<div class="project-title">
		<h2>The Magic Spell</h2>
		<h3>Web Design - 2011</h3>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/TheMagicSpell/TheMagicSpell-Homepage.png" class="image-responsive">
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			
			<p>
				Near and dear to my heart is <a href="http://themagicspell.com/" target="_blank">themagicspell.com</a> for a few reasons. To begin with, this is the first website I ever designed. More importantly, the website was created for my mom who wrote the children's book 'Mable's Magic Spell.'
			</p>

			<p>
				And while it could use some updates to make it more modern (which I'll be happy to do if she wants to get back to it) I still find it a fun touchstone of where, in some ways a lot of this began for me.
			</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
			<div class="builtWithSimple">
				<img src="<?=$cdn_url ?>/builtWith/HTML.png" class="built-with-icon"><p>HTML</p>
			</div>

			<div class="builtWithSimple">
				<img src="<?=$cdn_url ?>/builtWith/CSS.png" class="built-with-icon"><p>CSS</p>
			</div>
		</div>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/TheMagicSpell/TheMagicSpell-word-list.png" class="work-column left" />
	<img src="<?=$cdn_url ?>/TheMagicSpell/TheMagicSpell-sample-pages-from-the-book.png" class="work-column right" />
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Mable's Magic Spell",
		"page_description" : "Themagicspell.com is very near and dear to my heart. It was one of the first websites I ever created and helped launch my interest in web development and design as a career profession. It is also for a book written by my mother. Making it extra special."
	}
</script>
