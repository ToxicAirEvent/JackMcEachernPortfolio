<div class="container">
	<div class="project-title">
		<h2>Boland Energy</h2>
		<h4>Wordpress Theming - 2019</h4>
	</div>
</div>


<div class="expanded-work">
	<img src="<?=$cdn_url ?>/BolandEnergy/HomePage2020.png" class="image-responsive" />
</div>


<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p>Located in Beaufort Missouri, <a href="https://www.bolandenergy.com/" target="_blank">Boland Energy</a> is a family owned and operated propane, propane tank, gasoline, diesel fuel, and lubricants provider. There website was originally built with wordpress but it had been many years since it saw any major revisions.</p>

			<p>With the existing theme lacking consistent navigation, fonts choices, responsive mobile formatting as well as a color scheme inline with their brand the decision was made to complete scrap the existing theme and start over. The existing written content was however retained as it was considered up to date, accurate, and ownership did not feel the need to rewrite it at this time.</p>

			<p>Boland Energy brought in a third-party photographer in the process of the redesign which allowed for high quality images to be placed with the supporting text. Allowing some new life to be given to the existing written content.</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
			<img src="<?=$cdn_url ?>/builtWith/WordpressLogo.png" class="img-responsive">
		</div>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/BolandEnergy/ContactUs2020.png" class="work-column left">
	<img src="<?=$cdn_url ?>/BolandEnergy/FAQ2020.png" class="work-column left">
	<img src="<?=$cdn_url ?>/BolandEnergy/ServicesAndMaintenance2020.png" class="work-column right">
	<img src="<?=$cdn_url ?>/BolandEnergy/BuyingATank2020.png" class="work-column right">
	<img src="<?=$cdn_url ?>/BolandEnergy/ProductsAndServices2020.png" class="work-column left">
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Boland Energy",
		"page_description" : "Boland Energy was given a website redesign in 2019 by Jack McEachern in an effort to update the existing design while retaining the existing written content."
	}
</script>
