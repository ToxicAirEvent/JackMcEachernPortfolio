<div class="container">
	<div class="project-title">
		<h2>Graphic, Logo, and Print Design</h2>
		<h3>Graphic Design - 2015 to Present</h3>
	</div>

	<div class="project-details-container breathing-room">
		<h5>About:</h5>
		<p>
			Going hand in hand with designing websites and shaping user experiences I'm often left needing to make some of my own graphical assets for projects, and in a few cases have been tasked with creating entirely new logos to represent the clients wishes.
		</p>

		<p>
			Below is a collection of graphics I have made over the years as a part of my freelance work, schooling, or just personal interest/pet projects.
		</p>
	</div>
</div>


<div class="expanded-work" id="content">
	<?php 
	$images = [
	"Eco-Terra-Globe.png",
	"Jack-McEachern---Font-Complete.jpg",
	"The Bellary - Fake Band poster-alt.jpg",
	"OrderlyNewsIcon-Large.png",
	"Xtreme_Outdoors_Logo_-_Cut Sheat_full-01.png",
	"Armchair_Analyst_Logo_Options.jpg",
	"JM-Logo.png",
	"M_Jack_mixerBrush.jpg",
	"SI-BaseCover-Final.png",
	];
	
	foreach($images as $image): ?>
		<img src="<?=$cdn_url ?>/AssortedGraphicDesign/<?= $image ?>" class="image-responsive scale_height project-images">
	<?php endforeach; ?>
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Graphic, Logo, and Print Design",
		"page_description" : "Over the years Jack McEachern has worked with a number of clients to create unique graphic designs. Here is a sample of a few of his works. Learn more and view."
	}
</script>
