<div class="container" id="404">
	<h1>404.</h1>
	<h3>Seems you've hit a snag.</h3>
	<p><a href="<?=$site_url ?>">Go home if you want.</a></p>
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "404! Oops."
	}
</script>