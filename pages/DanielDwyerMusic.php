<div class="container">
	<div class="project-title">
		<h2>Daniel Dwyer Music</h2>
		<h3>Web Design, and Graphic Design - 2014</h3>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/DDMusic/HomePage2020.png" class="image-responsive" />
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p>Daniel Dwyer Music, a musician, wanted a web presence as he released his first album <em>Sincerely,</em>.  The theme is simple and minimal to match the cover of his EP.</p>
			<p>Daniel has since moved on to other endeavors and no longer maintains the site but <a href="<?=$site_url ?>/ddmusic/index.html" target="_blank">an archive is kept alive.</a></p>
			<p>Being so simple and straight forward in content the decision was made to simply do this website as static content. Handwritten with just good old html and css. With a bunch of iframe embeds to SoundCloud and Facebook content.</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
			<div class="builtWithSimple">
				<img src="<?=$cdn_url ?>/builtWith/HTML.png" class="built-with-icon"><p>HTML</p>
			</div>

			<div class="builtWithSimple">
				<img src="<?=$cdn_url ?>/builtWith/CSS.png" class="built-with-icon"><p>CSS</p>
			</div>
		</div>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/DDMusic/About.png" class="image-responsive" />
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Daniel Dwyer Music",
		"page_description" : "Daniel Dwyer is a musician in the St. Louis area working on a variety of projects with a number of different artist. For his EP Sincery, he needed a simple and minimal website to match the albums cover art. View the website and learn more."
	}
</script>
