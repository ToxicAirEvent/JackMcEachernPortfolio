<div class="container">
	<div class="project-title">
		<h2>Museum of Transportation History In Photos</h2>
		<h3>Video and Photo Editing - 2016</h3>
	</div>

	<div class="project-details-container breathing-room">
		<h5>About:</h5>
		<p>This simple video slideshow created for <a href="http://transportmuseumassociation.org/" target="_blank">The National Museum of Transportation</a> in St. Louis Missouri.</p>
	<p>Image corrections and touchups were done using Adobe Photoshop. Editing and sequencing was done using Adobe Premiere Pro.</p>
	</div>
</div>

<div class="expanded-work" id="content">
	<iframe width="1512" height="850" src="https://www.youtube-nocookie.com/embed/KJ__f-mA6Yk?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Museum of Transportation History In Photos",
		"page_description" : "A photo slide show made for the National Museum of Transportation by Jack McEachern with the purpose of showing the history of the museum. Leanr more and watch."
	}
</script>