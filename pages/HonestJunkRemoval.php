<div class="container">
	<div class="project-title">
		<h2>Honest Junk Removal</h2>
		<h4>Web Design, &amp; Wordpress Theming - 2017</h4>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/HonestJunk/HomePage2020.png" class="image-responsive" />
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p>The owners of <a href="https://www.honestjunk.com/" target="_blank">Honest Junk Removal</a>, a St. Louis based small business, wanted a quick and clean website rebuild on top of their existing Wordpress installation, with a theme developed in such a way that they could make small content tweaks themselves without having to contact a web developer.</p>  
			<p><a href="https://www.honestjunk.com/" target="_blank">Honest Junk Removal</a> prides themselves on being a no frills, straight to the point professional junk removal service with flexible scheduling, pricing, and courteous staff members. With the primary driver of their business being quote request since every job is highly variable in the work and time necessary to complete it.</p>
			<p>This was the primary driver in making the theme fairly bare bones, yet neatly constructed with blocks of color and consistent typography. With a high repition of contact information either by form submission for a call back later, or a direct line to the owners.</p>
			<p>For quotes submitted via the embeded form the data is sent via a wordpress extension directly to both owners emails with images attached allowing them to easily manage and review all incoming quotes from a single inbox.</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
			<img src="<?=$cdn_url ?>/builtWith/WordpressLogo.png" class="img-responsive">
		</div>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/HonestJunk/RemovalServices.png" class="work-column left">
	<img src="<?=$cdn_url ?>/HonestJunk/ContactUs.png" class="work-column left">
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Honest Junk Removal",
		"page_description" : "Honest Junk Removal is a local junk and garbage removal service in St, Louis, Missouri. Jack McEachern updated their wordpress theme to give the site a more modern design and additional functionality."
	}
</script>
