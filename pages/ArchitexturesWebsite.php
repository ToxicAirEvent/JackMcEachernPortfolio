<div class="container">
	<div class="project-title">
		<h2>Architextures</h2>
		<h4>Web & Graphic Design - 2014</h4>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/Architextures/Architextures-Home-2020.png" class="image-responsive" />
</div>

<div class="container">
	<div class="project-details-container breathing-room">
		<div class="project-description">
			<h5>About The Project:</h5>
			<p>In the summer of 2014 I had the pleasure of interning at Architextures LLC in Webster Groves, Missouri.</p>
			<p>
				The primary goal for the internship was to replace the companies dated flash based website with something more modern.
				The end result was a simple, static site that relied heavily on showcasing the work portfolio across a variety of different interior and exterior spaces. Effectively acting as a brochure of the services and design styles they provide as well as some explanation of their companies philosophies.
			</p>
			<p>In addition to being responsible for the website redesign I assisted with portions of the photography featured on the website as well.</p>
			<p>You can see the full site at <a href="http://www.architexturesllc.com/" target="_blank">architexturesllc.com</a> or view selected screenshots below.</p>
		</div>

		<div class="project-details">
			<h5>Build With:</h5>
			<div class="builtWithSimple">
				<img src="<?=$cdn_url ?>/builtWith/HTML.png" class="built-with-icon"><p>HTML</p>
			</div>

			<div class="builtWithSimple">
				<img src="<?=$cdn_url ?>/builtWith/CSS.png" class="built-with-icon"><p>CSS</p>
			</div>
		</div>
	</div>
</div>

<div class="expanded-work">
	<img src="<?=$cdn_url ?>/Architextures/Architextures-Work-2020.png" class="work-column left" />
	<img src="<?=$cdn_url ?>/Architextures/Architextures-Team-2020.png" class="work-column right" />
	<img src="<?=$cdn_url ?>/Architextures/Architextures-About-2020.png" class="work-column left" />
	<img src="<?=$cdn_url ?>/Architextures/Architextures-Giving-2020.png" class="work-column right" />
</div>

<script type="application/json" id="page_meta">
	{
		"page_title": "Architextures",
		"page_description" : "Architextures LLC is an architecture company in Webster Groves, Missouri. Jack McEachern completed a website redesign while working as an intern in the summer of 2014. He also assisted in producing new print collateral material."
	}
</script>
