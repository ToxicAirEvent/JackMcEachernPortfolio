$(window).on("load", function() {
	//See how wide the image gallery container is.
	var containerOffset = $('#topGallery').offset().left + $('#topGallery').width();
	var lastImageDistance = $('#topGallery img').last().offset().left + $('#topGallery img').last().width();

	//If the gallery is scrolled on we want to update the amount of the total scroll length complete on the progress bar.
	$('#topGallery').scroll(function(){
		scrollCompleted(containerOffset);
	});

	//Function calculates how far away the last image is from being completely in view and updates progress bar accordingly.
	function scrollCompleted(mainContainerOffset){
		//See how far away the last image in the gallery is from being completely within the gallery.
		var lastImage = $('#topGallery img').last().offset().left + $('#topGallery img').last().width();

		var imageDifFromRight = lastImage - mainContainerOffset;
		var percentScrolled = 100 - ((imageDifFromRight/mainContainerOffset) * 100);

		/*
			Some weird things can happen with borders and padding and we want the bar to always be visible.
			So if the number returned is less than 10 we always leave the progress at 10% showing.
			If the number if greater than 100 we cap the number at 100% progress.
		*/
		if(percentScrolled <= 5){
			percentScrolled = 5;
		}

		if(percentScrolled >= 100){
			percentScrolled = 100;
		}

		var sliderFillString = percentScrolled + "%";

		//Now that the math is done simply update and animate the width change.
		$('#sliderFill').width(sliderFillString);
	}

	//Allow for clicking and dragging on desktop using the TouchScroll library/extension.
	var viewer = new TouchScroll();
    viewer.init({
        id: 'topGallery',
        draggable: true,
        wait: false
    });

    var dragDetect = 0;

    $("#imageSlider").mousedown(function(){
    	console.log("Mouse down on slider.");
    	dragDetect = 1;
    });

    $("body").mouseup(function(){
    	console.log("Mouse released.");
    	dragDetect = 0;
    });

    //Allow clicking and dragging of the progress bar to jump to a specific point in the gallery.
    $("#imageSlider").mousemove(function(e){
    	if(dragDetect === 1){
	    	//Get the relative position
		   var parentOffset = $(this).parent().offset();
		   var relX = e.pageX - parentOffset.left;

		   var percentJump = relX / $(this).width();

		   var scrollPercentageJump = (lastImageDistance - containerOffset)* percentJump.toFixed(2);

		   $('#topGallery').scrollLeft(scrollPercentageJump);
		}
	});
})