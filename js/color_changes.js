$('.item').hover(function(){
	
	//Add color lock-in to the thing that is being hovered over.
	$(this).addClass('color-lock-in');

	/*
	Now that an item has been hovered lets loop over every item on the page.
	If the item is being hovered lets pull out the most prominent color for it and change the link to that.
	If the item is not being hovered lets just make it black and white.
	*/
	$('.item').each(function(){
		var item_text = $(this).find('h1').text();
		var item_id = "#" + $(this).attr('id');

		//Get the type of vibrant color to find within the image from the data element on the item.
		var vibrant_type = $(this).data('vibrant-type');
		
		//If there is no vibrant type just get the darkest color. This will be closest to black text which should be easy to read when in doubt.
		if (typeof vibrant_type === 'undefined' || vibrant_type === null) {
		    vibrant_type = 'DarkMuted';
		}

		if($(this).hasClass('color-lock-in')){

			/*
				Some images have a really hard time parsing the color they should contain.
				Skip was added to just assign a color to these pieces of text. That way hover feedback is still given
				but we also don't throw color parsing errors in Vibrant JS.
			*/
			if(vibrant_type === 'skip'){
				$(item_id).find('h1').css('color', '#42c5f4');
			}else{

			var image_url = $(this).find('img').attr('src');
			var image_text_color = Vibrant.from(image_url).getPalette(function(err, palette) {
					var image_text_color = palette[vibrant_type].getHex();

					//console.log("The vibrant type is: " + vibrant_type);
					//console.log("Text color will be: " + palette['LightVibrant'].getHex());
					$(item_id).find('h1').css('color', image_text_color);
				});
			}
			
		}else{
			$(this).find('img').addClass('item-image-black-and-white');
		}
		
	});
	
},
function(){
	//Remove these classes from everything when a particular item is no longer being hovered.
	$('.item').each(function(){
		$(this).removeClass('color-lock-in');
		$(this).find('img').removeClass('item-image-black-and-white');
		
		var item_id = "#" + $(this).attr('id');
		$(item_id).find('h1').css('color', 'black');
	});
});
