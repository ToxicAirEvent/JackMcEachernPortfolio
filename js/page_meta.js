jQuery(document).ready(function($) {
	if($('#page_meta').length){

		page_data = jQuery.parseJSON($('#page_meta').text());

		//If the page has a title go ahead and update that title.
		if(page_data.page_title != null){
			var current_title = document.title;
			var formed_title =  page_data.page_title + " - " + current_title;
			document.title = formed_title;
		}
		
		if(page_data.page_description != null){
			$('meta[name=description]').attr('content',page_data.page_description);
			console.log($('meta[name=description]').attr('content'));
		}
	}
});