/*
An image scaler that makes it so images that are extremely tall vertically don't become taller than the window. 
This way you can see them as a full item/object all at once.

Simply attach the scale_height class to any image and this plugin will scale the image properly.
It is advised you use this in conjunction with the image-responsive css class as well to stop things from getting really wonky.
*/

$(window).on("load", function() {
	var window_height = window.innerHeight;
	
	//Raw height is the value used for comparison operators. image_height_breathing_room contains the actual string for the height we want to set things to.
	var raw_height = window_height;
	console.log("Window height: " + raw_height);

	var image_height_breathing_room = window_height * .96;
	image_height_breathing_room = image_height_breathing_room + "px";
	console.log("Images should be " + image_height_breathing_room + " tall.");
	
	console.log("---------------------------------------");

	$('.scale_height').each(function(){
		
		var real_height = $(this).height();
		var img_url = $(this).attr('src');
		
		console.log("Image URL: " + img_url);
		console.log("Height before scaling: " + real_height);
		
		if(real_height <= raw_height){
			console.log("This image will not be scaled.");
		}else{
			$(this).css({'height' : image_height_breathing_room});
			console.log("Image height will be scaled to: " + image_height_breathing_room);
		}
		console.log("---------------------------------------");
	});
});