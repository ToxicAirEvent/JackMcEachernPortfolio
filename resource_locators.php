<?php

	/* Content Resources */
	$protocol = 'https://';
	$root_url = 'jackmceachern.com';
	$images_url = 's3.us-east-2.amazonaws.com/thecooltable/img';
	
	$site_url = $protocol . $root_url;
	$cdn_url = $protocol . $images_url;
	
	/* Common Content Strings */
	$main_site_title = "Jack McEachern Design";
	$default_meta_description = "Jack McEachern, a graphic and web designer, received a B.S. in Computer Graphics and Multimedia Design from Southeast Missouri State University in August, 2016. Jack designs websites, prepares marketing and social media materials, and always enjoys taking the next great photograph. Get in touch."
?>