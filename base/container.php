<? 
//See if the page variable is even set in the URL.
if(isset($_GET['page'])){
	$piece = $_GET['page'];
	
	$pages_dir = './pages';
	
	//Get all the files in the pages directory except for the .. and . unix characters.
	$page_list = array_diff(scandir($pages_dir), array('..', '.'));
	
	//Make a new array which will contain full page names with extensions and one without.
	$page_list_new = [];
	
	//Loop over each page, trim off its extension, and pair it with the page file.
	foreach($page_list as $page){
		$human_page = pathinfo($page, PATHINFO_FILENAME);
		
		$all_page_data = ["file_name" => $page,"human_name" => $human_page];
		
		array_push($page_list_new,$all_page_data);
	}
	
	//Search for the human readable name of any given page and return it's key value.
	$page_key_id = searchForPage($piece,$page_list_new);
	
	//If it can't be found 404.
	if($page_key_id === null){
		$page_file_name = "./pages/404.php";
	}else{
		$page_file_name = './pages/' . $page_list_new[$page_key_id]["file_name"];
	}
}else{
	//If the variable isn't set just get the home page.
	$page_file_name = './pages/home.php';
}

//Put up the page a user is requesting.
include($page_file_name);

//Search the array of page names for a match, returns the key. Returns null if now found.
function searchForPage($page, $array) {
   foreach ($array as $key => $val) {
       if ($val['human_name'] === $page) {
           return $key;
       }
   }
   return null;
}
?>