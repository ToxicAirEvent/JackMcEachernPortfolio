<div class="container" id="footer">
	<p><a href="/index.php">&larr; Back To All Work</a></p>
	<p>&copy; 2022 - Jack McEachern.</p>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?=$site_url ?>/js/page_meta.js"></script>
<script src="<?=$site_url ?>/js/vibrant.js"></script>
<script src="<?=$site_url ?>/js/touchscroll.js"></script>
<script src="<?=$site_url ?>/js/color_changes.js"></script>
<script src="<?=$site_url ?>/js/vertical_image_scaler.js"></script>
<!--<script src="<?=$site_url ?>/js/galleryManager.js"></script>-->
	
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-32585378-5', 'auto');
  ga('send', 'pageview');

</script>
		
</body>
</html>