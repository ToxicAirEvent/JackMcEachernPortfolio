<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

	<title><?=$main_site_title ?></title>
	<meta charset="UTF-8">
	<meta name="description" content="<?=$default_meta_description ?>">
	
	
	<link href="<?=$site_url ?>/css/style.css" rel="stylesheet">
</head>

<body>

<div class="container" id="header">
	<div class="header">
		<h3>User Experience Design & Web Development By</h3>
		<h1><a class="home_header_link" href="<?=$site_url ?>" title="Click to return home.">Jack McEachern</a></h1>
	</div>
</div>